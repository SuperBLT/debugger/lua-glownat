#pragma once

#include <cstdint>
#include <string>

/**
 * Created by znix on 11/6/18.
 */

class Idstring {
	uint64_t raw;

public:
	// Make a default constructor, otherwise everything is a PITA with structs
	Idstring() : Idstring(0) {}

	Idstring(uint64_t raw) : raw(raw) {} // NOLINT(google-explicit-constructor)

	bool operator<(const Idstring &other) const;

	uint64_t GetRaw() const { return raw; }

	void ToHex(char string[17]) const;

	std::string ToUserRep() const;
};
