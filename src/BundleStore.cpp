#include <utility>

/**
 * Created by znix on 11/6/18.
 */

#include "BundleStore.h"
#include "file_util.h"
#include "Idstring.h"
#include "dsl/DslVector.h"

#include <fstream>
#include <vector>
#include <dsl/DslFile.h>

using namespace std;

void BundleStore::LoadBundleDatabase(const char *path) {
	ifstream in;
	in.exceptions(ifstream::failbit | ifstream::badbit);
	in.open(path, std::ios::binary);

	// The first thing in the file is a pointer (probably a vtable, which gets set during loading), and
	//  it's supposed to be zero. If it's not when read as a long, we're loading a 32-bit file, as it'll
	//  be overlapping the size value on the languages list.
	is32bit = read<uint64_t>(in) != 0;

	// Rewind now we've found the file's type
	in.seekg(0);

	// That first vtable again
	fassert(readPointer(in, is32bit) == 0);

	std::map<int, const DslLang*> dslLangs;
	DslVector languages(in, is32bit);
	languages.ReadAll(in, 0, 16, [&dslLangs](istream &in) {
		DslLang *lang = new DslLang{};
		lang->name = read<Idstring>(in);
		lang->id = read<int>(in);
		fassert(read<int>(in) == 0); // padding?
		dslLangs[lang->id] = lang;
	});

	// Appears to be a sortmap vector - afaik it's just used as padding, and written over by the loader
	in.seekg(PTR_LEN * 2, ios_base::cur);

	DslVector filesDsl(in, is32bit);

	tempHeaders.resize(filesDsl.GetSize());

	filesDsl.ReadAll(in, 0, 32, [this, &dslLangs](istream &in) {
		DslFile header;

		header.type = read<Idstring>(in);
		header.name = read<Idstring>(in);

		auto langID = read<unsigned int>(in);
		if (dslLangs.count(langID))
			header.lang = dslLangs[langID];
		fassert(read<int>(in) == 0);
		header.fileID = read<unsigned int>(in);
		fassert(read<int>(in) == 0);

		fassert(header.fileID >= 1);
		fassert(header.fileID <= tempHeaders.size());

		tempHeaders[header.fileID - 1] = header;
	});

	// printf("Files: %d\n", filesDsl.GetSize());
	// fflush(stdout);
}

void BundleStore::LoadMultiHeader(const char *path) {
	ifstream in;
	in.exceptions(ifstream::failbit | ifstream::badbit);
	in.open(path, std::ios::binary);

	auto length = read<uint32_t>(in);

	DslVector bundlesDsl(in, is32bit);
	bundlesDsl.ReadAll(in, 4, is32bit ? 28 : 48, [&](istream &in) {
		auto bundleId = read<unsigned int>(in);

		// AFAIK this is just padding, until the next word alignment
		fassert(read<int>(in) == 0);
		if(!is32bit) {
			fassert(read<int>(in) == 0);
			fassert(read<int>(in) == 0);
		}

		DslVector entriesDsl(in, is32bit);
		entriesDsl.ReadAll(in, 4, 12, [&](istream &in) {
			auto fileId = read<uint32_t>(in);

			DslFile &header = tempHeaders[fileId - 1];
			header.bundleID = bundleId;
			header.offset = read<uint32_t>(in);
			header.length = read<uint32_t>(in);
		});

		// Who knows what this is for
		fassert(readPointer(in, is32bit) == 1);
	});
}

void BundleStore::LoadSingleHeader(const char *path) {
	ifstream in;
	in.exceptions(ifstream::failbit | ifstream::badbit);
	in.open(path, std::ios::binary);

	// Ensure we haven't already processed this bundle
	for (const auto &pair : bundles) {
		fassert(pair.second != path);
	}

	unsigned int bundleID = nextSingleBundleId++;
	bundles[bundleID] = path;

	auto length = read<uint32_t>(in);

	DslVector entriesDsl(in, is32bit);

	DslFile *last = nullptr;

	entriesDsl.ReadAll(in, 4, 8, [&](istream &in) {
		auto fileId = read<uint32_t>(in);
		auto offset = read<uint32_t>(in);

		DslFile &header = tempHeaders[fileId - 1];
		header.bundleID = bundleID;
		header.offset = offset;

		// Note we don't know the length for these files - same as PD2, we need to
		//  calculate it based on the position of the next file, or the end of the bundle
		//  in the case of the last element.
		//
		// For the last file, don't set the length and let the application read until the end of the file

		if(last) {
			last->length = offset - last->offset;
		}

		last = &header;
	});

	// TODO footer stuff
}

void BundleStore::UpdateTree() {
	tree = make_unique<BundleTree>(tempHeaders);

	// TODO clean up tempHeaders, and ensure the loading methods aren't called again by accident
}
