#pragma once

#include <Idstring.h>

/**
 * Created by znix on 11/7/18.
 */

struct DslLang {
public:
	int id;
	Idstring name;
};

struct DslFile {
public:
	Idstring name;
	Idstring type;
	unsigned int fileID{};
	const DslLang *lang = nullptr;

	// These are used for reading, and are picked up from the bundle headers
	unsigned int bundleID = ~0u;
	unsigned int offset = ~0u;
	unsigned int length = ~0u;

	bool Found() const {
		return bundleID != ~0u;
	}

	bool HasLength() const {
		return length != ~0u;
	}
};
