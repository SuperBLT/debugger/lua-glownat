/**
 * Created by znix on 11/6/18.
 */

#include "DslVector.h"
#include "file_util.h"

using namespace std;

DslVector::DslVector(istream &in, bool is32bit) {
	size = read<uint32_t>(in);

	// Ensure the capacity is at least as large as the size. It's usually the same, but sometimes isn't.
	fassert(read<uint32_t>(in) >= size);

	offset = readPointer(in, is32bit);

	// Allocator pointer, again it's always saved as zero
	fassert(readPointer(in, is32bit) == 0);
}

void DslVector::ReadAll(istream &in, size_t offset, size_t entry_size, function<void (istream&)> reader) {
	streampos start_pos = in.tellg();

	size_t vec_start = this->offset + offset;
	in.seekg(vec_start);

	for (int i = 0; i < size; i++) {
		reader(in);
		fassert(size_t(in.tellg()) == vec_start + entry_size * (i + 1));
	}

	// Go back to where we were
	in.seekg(start_pos);
}
