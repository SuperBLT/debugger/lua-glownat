#pragma once

#include <iosfwd>
#include <cstdint>
#include <functional>

/**
 * Created by znix on 11/6/18.
 */

class DslVector {
public:
	DslVector(std::istream &in, bool is32bit);

	inline unsigned int GetSize() { return size; }

	void ReadAll(std::istream &in, size_t offset, size_t size, std::function<void (std::istream &)>);

private:
	unsigned int size;
	uint64_t offset;
};
