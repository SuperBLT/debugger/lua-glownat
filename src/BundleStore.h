#pragma once

#include <map>
#include <memory>

#include "dsl/DslFile.h"
#include "BundleTree.h"

/**
 * Created by znix on 11/6/18.
 */

class BundleStore {
public:
	static BundleStore &Instance() {
		static BundleStore instance;
		return instance;
	}

	void LoadBundleDatabase(const char *path);
	void LoadMultiHeader(const char *path);
	void LoadSingleHeader(const char *path);
	void UpdateTree();

	BundleTree *GetTree() { return tree.get(); }

	std::string GetBundleById(unsigned int id) { return bundles[id]; }

private:
	bool is32bit{};
	std::vector<DslFile> tempHeaders;

	unsigned int nextSingleBundleId = 10000;
	std::map<unsigned int, std::string> bundles;

	std::unique_ptr<BundleTree> tree;
};
