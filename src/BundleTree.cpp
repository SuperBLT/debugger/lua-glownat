/**
 * Created by znix on 11/7/18.
 */

#include "BundleTree.h"
#include "HashList.h"
#include "file_util.h"

#include <utility>

using namespace std;

class TempDir {
public:
	TempDir() {
		dirChildren.reserve(10);
		fileChildren.reserve(100);
	}

	TempDir(string name, TempDir *parent) : name(std::move(name)), parent(parent) {}

	string name;
	vector<TempDir *> dirChildren;
	vector<DslFile> fileChildren;
	TempDir *parent = nullptr;

	string GetPath() {
		if (!parent || parent->name.empty())
			return name;

		return parent->GetPath() + "/" + name;
	}

	size_t GetSize() {
		size_t size = sizeof(DslFile) * fileChildren.size() + sizeof(BundleTree::BundleDirectory);

		for (TempDir *dir : dirChildren) {
			size += dir->GetSize();
		}

		return size;
	}

	BundleTree::BundleDirectory *WriteRoot(uint8_t *data, size_t *used_out) {
		size_t used = 0;

		used += WriteFiles(data + used);
		used += WriteChildren(data + used);

		auto *thisPos = (BundleTree::BundleDirectory *) (data + used);
		used += DoWrite(data + used);

		if (used_out)
			*used_out = used;

		return thisPos;
	}

private:
	size_t WriteFiles(uint8_t *data) {
		fassert(!writingFiles);

		size_t used = 0;

		// First prepare each subdirectory
		for (TempDir *dir : dirChildren) {
			used += dir->WriteFiles(data + used);
		}

		// Then write out each file
		writingFiles = (DslFile *) (data + used);
		for (const DslFile &file : fileChildren) {
			*(DslFile *) (data + used) = file;
			used += sizeof(DslFile);
		}

		return used;
	}

	size_t WriteChildren(uint8_t *data) {
		fassert(!writingDirs);

		size_t used = 0;

		// First write out each subdirectory's children
		for (TempDir *dir : dirChildren) {
			used += dir->WriteChildren(data + used);
		}

		// Then write out the subdirs themselves
		writingDirs = (BundleTree::BundleDirectory *) (data + used);
		for (TempDir *dir : dirChildren) {
			used += dir->DoWrite(data + used);
		}

		return used;
	}

	size_t DoWrite(uint8_t *data) {
		fassert(writingDirs);
		fassert(writingFiles);

		auto dir = (BundleTree::BundleDirectory *) (data);
		dir->name = HashList::getInstance().Register(name.c_str());
		dir->path = HashList::getInstance().Register(GetPath().c_str());
		dir->dirs = writingDirs;
		dir->dirsCount = static_cast<unsigned int>(dirChildren.size());
		dir->files = writingFiles;
		dir->filesCount = static_cast<unsigned int>(fileChildren.size());

		return sizeof(BundleTree::BundleDirectory);
	}

	BundleTree::BundleDirectory *writingDirs = nullptr;
	DslFile *writingFiles = nullptr;
};

static TempDir *resolveParent(TempDir &root, map<string, TempDir *> &dirCache, const string &path) {
	size_t lastSlash = path.find_last_of('/');

	if (lastSlash == string::npos) {
		return &root;
	}

	string parentPath = path.substr(0, lastSlash);

	if (dirCache.count(parentPath)) {
		return dirCache[parentPath];
	}

	// Parent doesn't exist, find where the to-be-created directory should live
	TempDir *ppDir = resolveParent(root, dirCache, parentPath);

	// Find the filename (without the path)
	lastSlash = parentPath.find_last_of('/');
	string name = parentPath;

	if (lastSlash != string::npos) {
		name.erase(0, lastSlash + 1);
	}

	TempDir *createdParent = new TempDir(name, ppDir);
	fassert(createdParent->GetPath() == parentPath);
	dirCache[parentPath] = createdParent;
	ppDir->dirChildren.push_back(createdParent);

	return createdParent;
}

BundleTree::BundleTree(const vector<DslFile> &files) {
	TempDir root;
	map<string, TempDir *> dirCache;

	for (const DslFile &fi : files) {
		fassert(fi.Found());

		Idstring nameId = fi.name;

		const char *name = HashList::getInstance().Lookup(nameId);
		if (!name) {
			// TODO should we put these somewhere else?
			root.fileChildren.push_back(fi);
			continue;
		}

		TempDir *parent = resolveParent(root, dirCache, name);
		parent->fileChildren.push_back(fi);
	}

	// Make sure we have all the files
	size_t i = root.fileChildren.size();
	for (const auto &pair : dirCache) {
		i += pair.second->fileChildren.size();
	}
	//fassert(i == files.size());

	// Assume each directory's hashes will consume around 200 bytes (between both the name and the path)
	HashList::getInstance().Reserve(dirCache.size() * 200);

	// Write it all out
	size_t size = root.GetSize();
	data = new uint8_t[size];
	size_t used = 0;
	this->root = root.WriteRoot(data, &used);
	fassert(size == used);

	// Free everything up
	root.dirChildren.clear();
	for (const auto &pair : dirCache) {
		delete pair.second;
	}

	HashList::getInstance().Reserve(-1);
}

BundleTree::~BundleTree() {
	delete[] data;
}
