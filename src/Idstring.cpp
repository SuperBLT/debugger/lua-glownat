/**
 * Created by znix on 11/6/18.
 */

#include "Idstring.h"
#include "HashList.h"
#include <cstdio>

#include <inttypes.h>

bool Idstring::operator<(const Idstring &other) const {
	return raw < other.raw;
}

void Idstring::ToHex(char *string) const {
	snprintf(string, 17, "%016" PRIx64, raw);
}

std::string Idstring::ToUserRep() const {
	const char *str = HashList::getInstance().Lookup(*this);

	if(str)
		return str;

	char buff[17];
	ToHex(buff);

	return buff;
}
