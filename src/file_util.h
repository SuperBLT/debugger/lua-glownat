#pragma once

#include <cstdint>
#include <exception>
#include <fstream>

/**
 * Created by znix on 11/7/18.
 */

#define S(x) #x
#define S_(x) S(x)
#define S__LINE__ S_(__LINE__)
#define fassert(cond) if(!(cond)) { throw FormatAssertion("GlowNat Assertion: line " __FILE__ ":" S__LINE__ ": " #cond); }

#define PTR_LEN (is32bit ? 4 : 8)
#define VEC_LEN (4*2 + PTR_LEN*2)

struct FormatAssertion : std::exception {
	const char *msg;

	explicit FormatAssertion(const char *msg) : msg(msg) {}

	const char *what() const noexcept override { return msg; }
};

template<typename T>
static T read(std::istream &in) {
	T val;
	in.read(reinterpret_cast<char *>(&val), sizeof(val));
	return val;
}

static uint64_t readPointer(std::istream &in, bool is32bit) {
	if (is32bit)
		return read<uint32_t>(in);
	else
		return read<uint64_t>(in);
}
