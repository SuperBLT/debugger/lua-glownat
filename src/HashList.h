#pragma once

#include <string>
#include <vector>
#include <map>
#include "Idstring.h"

/**
 * Created by znix on 11/6/18.
 */

class HashList {
public:
	static HashList &getInstance();

private:
	HashList() = default;

	struct entry {
		ptrdiff_t strtable_offset;

		entry() = default;

		explicit entry(ptrdiff_t offset) : strtable_offset(offset) {}

		const char *GetString(const HashList &list) {
			return list.stringPool.data() + strtable_offset;
		}
	};

	std::map<Idstring, entry> entries;
	std::vector<char> stringPool;

public:

	static Idstring hash(const char *);

	/**
	 * Register a hash, ensuring we can un-hash it later.
	 *
	 * @return The hash of the string
	 */
	Idstring Register(const char *);

	/**
	 * Look up the string corresponding to the given hash
	 *
	 * @retun The matching string, or {@code nullptr} if it is unknown
	 */
	const char *Lookup(Idstring);

	/**
	 * Ensures there are {@code count} unused bytes in the string pool, to greatly improve
	 * performance when registering a large number of strings.
	 *
	 * If {@code count} is -1, the string pool is trimmed to it's capacity - use this if
	 * you've allocated more memory than needed, as it's faster to allocate more than
	 * trim it down than to do the reverse.
	 */
	void Reserve(long count);
};
