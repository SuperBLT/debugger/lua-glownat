#pragma once

#include <vector>
#include <map>
#include <cstddef>
#include "dsl/DslFile.h"

/**
 * Created by znix on 11/7/18.
 */

class BundleTree {
public:
	enum EntryType {
		T_DIR,
		T_FILE
	};

	struct BundleDirectory {
		Idstring name;
		Idstring path;
		unsigned int dirsCount;
		unsigned int filesCount;
		BundleDirectory *dirs;
		DslFile *files;
	};

public:
	explicit BundleTree(const std::vector<DslFile> &files);

	BundleTree(const BundleTree &) = delete;

	~BundleTree();

	BundleDirectory *GetRoot() { return root; }

private:
	BundleDirectory *root = nullptr;
	uint8_t *data = nullptr;
};
