/**
 * Created by znix on 11/6/18.
 */

#include <cstring>
#include "HashList.h"
#include "Idstring.h"
#include "idstring_hash.h"


Idstring HashList::hash(const char *str) {
	return {idstring_hash((uint8_t *) str, strlen(str))};
}

Idstring HashList::Register(const char *str) {
	Idstring id = hash(str);

	entries[id] = entry(stringPool.size());

	// Also want to copy the null
	size_t length = strlen(str) + 1;

	stringPool.reserve(stringPool.size() + length);
	std::copy(str, str + length, std::back_inserter(stringPool));

	return id;
}

const char *HashList::Lookup(Idstring id) {
	if (!entries.count(id))
		return nullptr;

	return entries[id].GetString(*this);
}

HashList &HashList::getInstance() {
	static HashList inst;
	return inst;
}

void HashList::Reserve(long count) {
	if(count == -1) {
		stringPool.shrink_to_fit();
		return;
	}

	stringPool.reserve(stringPool.size() + count);
}
