#include "adv_load.h"

// Heavily derived from Lua's loadlib.c

#define LIBPREFIX       "ADV_LOADLIB: "
#define LIB_FAIL        "open"

/* error codes for ll_loadfunc */
#define ERRLIB          1
#define ERRFUNC         2

#define MAX_LOAD_ARGS 3

////

extern "C" {
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
}

#if defined(_WIN32)

#include <Windows.h>

static void pusherror(lua_State *L) {
	int error = GetLastError();
	char buffer[128];
	if (FormatMessageA(FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, error, 0, buffer, sizeof(buffer), NULL))
		lua_pushfstring(L, "system error %d: %s\n", error, buffer);
	else
		lua_pushfstring(L, "system error %d\n", error);
}

static void ll_unloadlib(void *lib) {
	FreeLibrary((HINSTANCE)lib);
}


static void *ll_load(lua_State *L, const char *path) {
	DWORD flags = 0;

	char buff[8192];

	if (!lua_isnoneornil(L, 3)) {
		luaL_checktype(L, 3, LUA_TTABLE);

		lua_getfield(L, 3, "w32_alt_load_path");
		if (lua_toboolean(L, -1)) {
			flags = LOAD_WITH_ALTERED_SEARCH_PATH;

			DWORD len = GetFullPathNameA(path, sizeof(buff), buff, NULL);
			if (len >= sizeof(buff))
				luaL_error(L, "buffer too small for GetFullPathNameA");

			if (len == 0) {
				pusherror(L);
				return NULL;
			}

			path = buff;
		}
	}

	HINSTANCE lib = LoadLibraryExA(path, NULL, flags);
	if (lib == NULL) pusherror(L);
	return lib;
}


static lua_CFunction ll_sym(lua_State *L, void *lib, const char *sym) {
	lua_CFunction f = (lua_CFunction)GetProcAddress((HINSTANCE)lib, sym);
	if (f == NULL) pusherror(L);
	return f;
}


static int ll_preload(lua_State *L) {
	const char *path = luaL_checkstring(L, 1);

	// No function is provided
	lua_pushnil(L);
	lua_insert(L, 2);

	if (lua_gettop(L) < MAX_LOAD_ARGS)
		lua_settop(L, MAX_LOAD_ARGS);

	HINSTANCE handle = (HINSTANCE)ll_load(L, path);

	if (!handle) {
		lua_pushnil(L);
		lua_insert(L, -2);
		return 2;
	}

	HINSTANCE *plib = (HINSTANCE*)lua_newuserdata(L, sizeof(HINSTANCE));
	*plib = handle;

	return 1;
}

static int ll_preload_release(lua_State *L) {
	luaL_checktype(L, 1, LUA_TUSERDATA);
	HINSTANCE *plib = (HINSTANCE *)lua_touserdata(L, 1);

	ll_unloadlib(*plib);

	return 0;
}

#else

#include <dlfcn.h>

// Work with Lua 5.1 (what ZeroBrane uses) even when compiled against newer Lua headers
// TODO do something with CMake to only use 5.1 headers and remove this
#define LUA_REGISTRYINDEX       (-10000)

static void ll_unloadlib (void *lib) {
	dlclose(lib);
}


static void *ll_load (lua_State *L, const char *path) {
	void *lib = dlopen(path, RTLD_NOW);
	if (lib == NULL) lua_pushstring(L, dlerror());
	return lib;
}


static lua_CFunction ll_sym (lua_State *L, void *lib, const char *sym) {
	lua_CFunction f = (lua_CFunction)dlsym(lib, sym);
	if (f == NULL) lua_pushstring(L, dlerror());
	return f;
}

#endif

static void **ll_register(lua_State *L, const char *path) {
	void **plib;
	lua_pushfstring(L, "%s%s", LIBPREFIX, path);
	lua_gettable(L, LUA_REGISTRYINDEX);  /* check library in registry? */
	if (!lua_isnil(L, -1))  /* is there an entry? */
		plib = (void **)lua_touserdata(L, -1);
	else {  /* no entry yet; create one */
		lua_pop(L, 1);
		plib = (void **)lua_newuserdata(L, sizeof(const void *));
		*plib = NULL;
		luaL_getmetatable(L, "_LOADLIB");
		lua_setmetatable(L, -2);
		lua_pushfstring(L, "%s%s", LIBPREFIX, path);
		lua_pushvalue(L, -2);
		lua_settable(L, LUA_REGISTRYINDEX);
	}
	return plib;
}


static int ll_loadfunc(lua_State *L, const char *path, const char *sym) {
	void **reg = ll_register(L, path);
	if (*reg == NULL) *reg = ll_load(L, path);
	if (*reg == NULL)
		return ERRLIB;  /* unable to load library */
	else {
		lua_CFunction f = ll_sym(L, *reg, sym);
		if (f == NULL)
			return ERRFUNC;  /* unable to find function */
		lua_pushcfunction(L, f);
		return 0;  /* return function */
	}
}


static int ll_loadlib(lua_State *L) {
	const char *path = luaL_checkstring(L, 1);
	const char *init = luaL_checkstring(L, 2);
	if (lua_gettop(L) < MAX_LOAD_ARGS)
		lua_settop(L, MAX_LOAD_ARGS);
	int stat = ll_loadfunc(L, path, init);
	if (stat == 0)  /* no errors? */
		return 1;  /* return the loaded function */
	else {  /* error; error message is on stack top */
		lua_pushnil(L);
		lua_insert(L, -2);
		lua_pushstring(L, (stat == ERRLIB) ? LIB_FAIL : "init");
		return 3;  /* return nil, error message, and where */
	}
}

void ll_push_adv_load(lua_State *L) {
	lua_newtable(L);

	lua_pushcfunction(L, ll_loadlib);
	lua_setfield(L, -2, "loadlib");

#if defined(_WIN32)
	lua_pushcfunction(L, ll_preload);
	lua_setfield(L, -2, "preload");

	lua_pushcfunction(L, ll_preload_release);
	lua_setfield(L, -2, "preload_release");
#endif
}
