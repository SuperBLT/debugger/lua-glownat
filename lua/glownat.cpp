#include "HashList.h"
#include "BundleStore.h"
#include "BundleTree.h"

#include "bundled_file.h"
#include "adv_load.h"

#include <fstream>
#include <algorithm>
#include <functional>

#include <cstring>

#if defined(__unix)
#include <stdlib.h>
#include <ftw.h>
#elif defined(_WIN32)
#include <Windows.h>
#endif

#if defined(_WIN32)
#define LUA_EXPORT extern "C" __declspec(dllexport)
#else
#define LUA_EXPORT extern "C" __attribute__ ((visibility ("default")))
#endif

extern "C" {

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

}

using namespace std;

// trim from start (in place)
static inline void ltrim(string &s) {
        s.erase(s.begin(), find_if(s.begin(), s.end(), [](int ch) {
                return !isspace(ch);
        }));
}

// trim from end (in place)
static inline void rtrim(string &s) {
        s.erase(find_if(s.rbegin(), s.rend(), [](int ch) {
                return !isspace(ch);
        }).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(string &s) {
        ltrim(s);
        rtrim(s);
}

// AFAIK this was changed between 5.1 and 5.3, and I'd like to keep compatibility with all of them
struct fReg {
	const char *name;
	lua_CFunction func;
};

static void handleExceptions(lua_State *L, const function<void()> &f) { 
        try { 
                f(); 
        } 
        catch (const std::ios_base::failure &fail) { 
                const char *msg = strerror(errno); 
		luaL_error(L, "C++ IO Error: %s", msg);
        } 
        catch (const std::exception &e) { 
		luaL_error(L, "C++ Exception: %s", e.what());
        } 
}

// Hashlist
int hl_register(lua_State *L) {
	const char *str = luaL_checkstring(L, 1);

	HashList::getInstance().Register(str);

	return 0;
}

int hl_loadfile(lua_State *L) {
	errno = ENOENT;

	handleExceptions(L, [L](){
		const char *chars = luaL_checkstring(L, 1);
		ifstream in;
		in.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		in.open(chars, ios_base::in);

		in.seekg(0, ios_base::end);
		long length = in.tellg();
		in.seekg(0, ios_base::beg);

		// Add 1kb, to account for any whitespace in the file
		HashList::getInstance().Reserve(length + 1024);

		// getline can set failbit on EOF, so don't treat that as an exception
		in.exceptions(std::ifstream::badbit);

		string line;
		while (getline(in, line)) {
			trim(line);
			HashList::getInstance().Register(line.c_str());
		}

		HashList::getInstance().Reserve(-1);
	});

	return 0;
}

static const struct fReg lib_hashlist[] = {
	{"register", hl_register},
	{"loadfile", hl_loadfile},
	{NULL, NULL}  /* sentinel */
};

////

int bl_load_db(lua_State *L) {
	handleExceptions(L, [L]() {
                const char *str = luaL_checkstring(L, 1);
                BundleStore::Instance().LoadBundleDatabase(str);
        });
	return 0;
}

int bl_load_all_header(lua_State *L) {
	handleExceptions(L, [L]() {
                const char *str = luaL_checkstring(L, 1);
                BundleStore::Instance().LoadMultiHeader(str);
        });
	return 0;
}

int bl_load_header(lua_State *L) {
	handleExceptions(L, [L]() {
                const char *str = luaL_checkstring(L, 1);
                BundleStore::Instance().LoadSingleHeader(str);
        });
	return 0;
}

int bl_update_tree(lua_State *L) {
	handleExceptions(L, [L]() {
                BundleStore::Instance().UpdateTree();
        });
	return 0;
}

static const struct fReg lib_bundle[] = {
	{"load_db", bl_load_db},
	{"load_all_header", bl_load_all_header},
	{"load_header", bl_load_header},
	{"update_tree", bl_update_tree},
	{NULL, NULL}  /* sentinel */
};

////

BundleTree::BundleDirectory &check_dir(lua_State *L, int index) {
	luaL_checktype(L, index, LUA_TLIGHTUSERDATA);
	return *(BundleTree::BundleDirectory*) lua_touserdata(L, index);
}

int tr_root(lua_State *L) {
	BundleTree *tree = BundleStore::Instance().GetTree();

	if(!tree) {
		luaL_error(L, "Tree not built, or out of date!");
	}

	lua_pushlightuserdata(L, tree->GetRoot());
	return 1;
}

int tr_dir_info(lua_State *L) {
	const auto &dir = check_dir(L, 1);

	lua_newtable(L);

	lua_pushstring(L, dir.name.ToUserRep().c_str());
	lua_setfield(L, -2, "name");

	lua_pushstring(L, dir.path.ToUserRep().c_str());
	lua_setfield(L, -2, "path");

	return 1;
}

int tr_list_dirs(lua_State *L) {
	const auto &dir = check_dir(L, 1);

	lua_newtable(L);

	for(size_t i=0; i<dir.dirsCount; i++) {
		BundleTree::BundleDirectory &d = dir.dirs[i];
		lua_pushlightuserdata(L, &d);

		const char *name = HashList::getInstance().Lookup(d.name);
		lua_setfield(L, -2, name);
	}

	return 1;
}

int tr_list_files(lua_State *L) {
	const auto &dir = check_dir(L, 1);

	lua_newtable(L);

	char hashname[17];

	for(size_t i=0; i<dir.filesCount; i++) {
		lua_newtable(L);

		const DslFile &d = dir.files[i];
		lua_pushlightuserdata(L, (void*) &d);
		lua_setfield(L, -2, "_native_file_ptr");

		const char *name = HashList::getInstance().Lookup(d.name);
		if(name == nullptr) {
			lua_pushboolean(L, true);
			lua_setfield(L, -2, "unknown_name");

			d.name.ToHex(hashname);
			name = hashname;
		}

		const char *end_name = name;
		for(const char *i = end_name; *i; i++) {
			if(*i == '/' && *(i+1))
				end_name = i + 1;
		}

		lua_pushstring(L, name);
		lua_setfield(L, -2, "path");

		lua_pushstring(L, end_name);
		lua_setfield(L, -2, "name");

		if (d.lang) {
			lua_pushstring(L, HashList::getInstance().Lookup(d.lang->name));
			lua_setfield(L, -2, "lang");
		}

		string type = d.type.ToUserRep();
		lua_pushstring(L, type.c_str());
		lua_setfield(L, -2, "type");

		lua_rawseti(L, -2, i + 1);
	}

	return 1;
}

int tr_get_extended_info(lua_State *L) {
	luaL_checktype(L, -1, LUA_TTABLE);

	lua_getfield(L, -1, "_native_file_ptr");
	luaL_checktype(L, -1, LUA_TLIGHTUSERDATA);

	auto *file = (DslFile*) lua_touserdata(L, -1);
	lua_pop(L, 1);

	string bundle;
	if(file->bundleID < 10000) {
		// These are the all_* bundles, and don't have a seperate file. Let the Lua work it out from the bundle ID.
	} else {
		bundle = BundleStore::Instance().GetBundleById(file->bundleID);
		bundle.erase(bundle.size() - 9, 2);

		lua_pushstring(L, bundle.c_str());
		lua_setfield(L, -2, "bundle_file");
	}

	lua_pushnumber(L, file->bundleID);
	lua_setfield(L, -2, "bundle_id");

	lua_pushnumber(L, file->fileID);
	lua_setfield(L, -2, "file_id");

	lua_pushnumber(L, file->offset);
	lua_setfield(L, -2, "offset");

	if(file->HasLength()) {
		lua_pushnumber(L, file->length);
		lua_setfield(L, -2, "length");
	}

	return 1;
}

static const struct fReg lib_tree[] = {
	{"root", tr_root},
	{"dir_info", tr_dir_info},
	{"list_dirs", tr_list_dirs},
	{"list_files", tr_list_files},
	{"get_extended_info", tr_get_extended_info},
	{NULL, NULL}  /* sentinel */
};

////

string create_temp_dir_impl(string base) {
#if defined(__unix)
	// See http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap08.html#tag_08_03
	const char *tmp_path = getenv("TMPDIR");

	if(!tmp_path)
		tmp_path = "/tmp";

	string fn_template = string(tmp_path) + "/zb-diesel-XXXXXX";
	char *temp_base = new char[fn_template.size() + 1];
	strncpy(temp_base, fn_template.c_str(), fn_template.size());
	temp_base[fn_template.size()] = 0;

	string result = mkdtemp(temp_base);
	delete[] temp_base;
	return result;
#elif defined(_WIN32)
	// Use the path argument passed in as the base, and delete the directories for
	//  any instances that have stopped (use files containing the PID for this)

	// Use the base arg
	if (base.back() != '/' && base.back() != '\\') {
		base = base + '/';
	}

	base = base + "_temporary";
	CreateDirectoryA(base.c_str(), NULL);

	return base;
#endif
}

class tmp_holder {
public:
	tmp_holder(const tmp_holder&) = delete;

	explicit tmp_holder(string base) : path(create_temp_dir_impl(base)) {}

	~tmp_holder() {
#if defined(__unix)
		auto deleter = [](const char *path, const struct stat *info, int flags, struct FTW *ftw_info) -> int {
			remove(path);
			return 0;
		};

		nftw(path.c_str(), deleter, 20, FTW_DEPTH | FTW_PHYS);
#elif defined(_WIN32)
		recursive_delete_w32(path);
#else
#error "Unsupported platform"
#endif
	}

#if defined(_WIN32)
	void recursive_delete_w32(string path) {
		// MessageBoxA(0, path.c_str(), "to delete dir", MB_OK);

		if (path == "" || path == "\\") {
			// Let's not do that.
			return;
		}

		string expr = path + "\\*.*";
		char str[MAX_PATH];

		//List files
		WIN32_FIND_DATA entry = { 0 };
		HANDLE find_handle = FindFirstFile(expr.c_str(), &entry);
		do {
			string fn = entry.cFileName;
			if (fn == "." || fn == ".." || fn == "")
				continue;

			string file = path + "\\" + entry.cFileName;
			DWORD Attributes = GetFileAttributes(file.c_str());
			if (Attributes & FILE_ATTRIBUTE_DIRECTORY) {
				recursive_delete_w32(path);
			} else {
				// MessageBoxA(0, file.c_str(), "to delete", MB_OK);
				remove(file.c_str());
			}
		} while (FindNextFile(find_handle, &entry));
		FindClose(find_handle);

		RemoveDirectory(path.c_str());
	}
#endif

	const string path;
};

static unique_ptr<tmp_holder> temporary_dir;
int get_temp_dir(lua_State *L) {
	if(!temporary_dir)
		temporary_dir = make_unique<tmp_holder>(luaL_checkstring(L, 1));
	lua_pushstring(L, temporary_dir->path.c_str());
	return 1;
}

int shutdown(lua_State *L) {
	temporary_dir.reset();
	return 0;
}

////

int cr_transform(lua_State *L) {
	string key = luaL_checkstring(L, 1);
	size_t fileLen = luaL_checkinteger(L, 2);
	size_t fileOffset = luaL_checkinteger(L, 3);

	size_t len = 0;
	const char *data_str = luaL_checklstring(L, 4, &len);
	const uint8_t *data_lua = (const uint8_t*) data_str;
	vector<uint8_t> data(data_lua, data_lua + len);

	if(fileOffset + len > fileLen) {
		luaL_error(L, "cannot decrypt past end of file (offset of %d + datalen of %d > supplied fileLen of %d)", fileOffset, len, fileLen);
	}

	// Credit goes to MMavipc for this snippet
	for (size_t i = 0; i < len; ++i) {
		size_t pos = i + fileOffset;
		int keyIndex = ((fileLen+pos)*7) % key.length();
		data[i] ^= key[keyIndex] * (fileLen-pos);
	}

	lua_pushlstring(L, (const char*) data.data(), data.size());

	return 1;
}

static const struct fReg lib_crypto[] = {
	{"transform", cr_transform},
	{NULL, NULL}  /* sentinel */
};

////

static void open_table(lua_State *L, const fReg *lib) {
	lua_newtable(L);

	for(const struct fReg *reg = lib; reg->name; reg++) {
		lua_pushcfunction(L, reg->func);
		lua_setfield(L, -2, reg->name);
	}
}

LUA_EXPORT int luaopen_glownat(lua_State *L) {
	lua_newtable(L);

	open_table(L, lib_hashlist);
	lua_setfield(L, -2, "hashlist");

	open_table(L, lib_bundle);
	lua_setfield(L, -2, "bundle");

	open_table(L, lib_tree);
	lua_setfield(L, -2, "tree");

	open_table(L, lib_crypto);
	lua_setfield(L, -2, "crypto");

	ll_push_adv_load(L);
	lua_setfield(L, -2, "adv_load");

	lua_pushcfunction(L, get_temp_dir);
	lua_setfield(L, -2, "get_temp_dir");

	lua_pushcfunction(L, shutdown);
	lua_setfield(L, -2, "shutdown");

	return 1;
}

